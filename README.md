# Springboot + service discovery + api gateway
microservices implementation

## Implemented:-
- Added 2 microservices
- service registry (eureka)
- api gateway (spring cloud apigateway)

TODO:-
- resilience4j
- spring security
- cloud config
- zipkin log tracing
